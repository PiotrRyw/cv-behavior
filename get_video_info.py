import os
import argparse
import logging
from typing import List, Dict

from moviepy.editor import VideoFileClip
import pandas as pd
from openpyxl import load_workbook


logging.basicConfig(level=logging.INFO)


def get_video_duration(file_path: str) -> int:
    """
    Parameters
    ----------
        file_path: str
            Path to video file in .mp4 format.
    Returns
    -------
        duration: int
            Duration of video clip in seconds.
    """
    video = VideoFileClip(file_path)
    duration = video.duration
    video.close()
    logging.info('{} file info saved.'.format(file_path))
    return duration


def format_time(seconds: int) -> str:
    """
    Parameters
    ----------
        seconds: int
            Duration of video in seconds.
    Returns
    -------
        Duration of video in minutes:seconds format.
    """
    minutes = int(seconds // 60)
    seconds = int(seconds % 60)
    return f"{minutes}:{seconds:02d}"


def get_video_files(path: str) -> List:
    """
    Parameters
    ----------
        path: str
            Path to directory with videos.
    Returns
    -------
        videos_with_paths: List[str]
            List of paths to files with mp4. extension from path string.
    """
    videos = [file for file in os.listdir(path) if file.endswith('.mp4')]
    videos_with_paths = [os.path.join(path,video) for video in videos]
    return videos_with_paths


def transform_video_data_to_df(path: str) -> pd.DataFrame:
    """
    Parameters
    ----------
        path: str
            Data to transform to pd.DataFrame object from get_video_files() function.
    Returns
    -------
        data: pd.DataFrame
            pd.DataFrame object with info to save.
    """
    file_paths = get_video_files(path)
    seconds = [get_video_duration(file) for file in file_paths]
    sec_min_format = [format_time(sec) for sec in seconds]
    data = {
        'file_paths': file_paths,
        'seconds': seconds,
        'min-sec': sec_min_format,
    }
    logging.info('Info saved to DataFrame.')
    return pd.DataFrame(data=data)


def save_video_info_to_excel(path: str) -> None:
    """
    Parameters
    ----------
        path: str
            Data to transform to pd.DataFrame object from get_video_files() function.
    """
    df = transform_video_data_to_df(path)
    excel_path = 'dataset_info.xlsx'
    writer = pd.ExcelWriter(excel_path, engine='openpyxl')
    df.to_excel(writer, sheet_name='videos')
    writer.save()
    writer.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Saves info about videos from specified directory to excel sheet.',
        usage="get_video_info.py [-h] --path /path/to/videos/dir",
    )
    parser.add_argument('--path', required=True)
    args = parser.parse_args()

    save_video_info_to_excel(path=args.path)
    logging.info('Finished')
